# 2019-ncov

Shared repo for Wuhan coronavirus discussion.

## Prediction (by Dr. LU Haonan) of Jan 30

| *MODEL* | *PREDICTED CUMULATIVE CASE COUNT*  |
| ------ | ------ |
| Exponential | 9000 |
| Logistic Reg (saturated at 25630) | 8360 | 
| Logistic Reg (saturated at 30000) | 8471 | 
| SIR (saturated at 25630) | 7500-8500 | 


## Other Related Repos
https://github.com/lispczz/pneumonia

https://github.com/BlankerL/DXY-2019-nCoV-Crawler

https://github.com/mai-lang-chai/Wuhan2020

https://3g.dxy.cn/newh5/view/pneumonia



## Data Download URL
[JSON] https://view.inews.qq.com/g2/getOnsInfo?name=wuwei_ww_area_counts

https://qianxi.baidu.com/?from=shoubai


